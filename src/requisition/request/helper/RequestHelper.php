<?php
/**
 * Description :
 * This class allows to define request helper class.
 * Request helper allows to provide features,
 * for request.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_library\requisition\request\helper;

use liberty_code\library\bean\model\DefaultBean;

use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\request\library\ToolBoxRequest;



class RequestHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Request sending information factory instance.
     * @var SndInfoFactoryInterface
     */
    protected $objRequestSndInfoFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SndInfoFactoryInterface $objRequestSndInfoFactory
     */
    public function __construct(
        SndInfoFactoryInterface $objRequestSndInfoFactory
    )
    {
        // Init properties
        $this->objRequestSndInfoFactory = $objRequestSndInfoFactory;

        // Call parent constructor
        parent::__construct();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string URL,
     * from specified route.
     *
     * Route argument array format:
     * @see ToolBoxRequest::getStrUrl() route argument array format.
     *
     * Route argument specification array format:
     * @see ToolBoxRequest::getStrUrl() route argument specification array format.
     *
     * Argument format:
     * @see ToolBoxRequest::getStrUrl() argument format.
     *
     * @param string $strRoute
     * @param null|array $tabRouteArg = null
     * @param null|array $tabRouteArgSpec = null
     * @param null|string|array $arg = null
     * @return null|string
     */
    public function getStrUrl(
        $strRoute,
        array $tabRouteArg = null,
        array $tabRouteArgSpec = null,
        $arg = null
    )
    {
        // Return result
        return ToolBoxRequest::getStrUrl(
            $this->objRequestSndInfoFactory,
            $strRoute,
            $tabRouteArg,
            $tabRouteArgSpec,
            $arg
        );
    }



}