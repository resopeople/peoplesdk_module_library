<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_library\requisition\library;



class ConstRequisition
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'people_library_requisition';



}