<?php

use people_sdk\library\requisition\request\info\factory\config\base\model\BaseConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\model\MultiSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                // Requisition request sending information factory
                'snd_info_factory' => [
                    /**
                     * Configuration array format:
                     * @see MultiSndInfoFactory configuration array format, for snd_info_factory.
                     */
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_base_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'base' => [
            'requisition' => [
                'request' => [
                    // Base requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * Null OR @see BaseConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'snd_info_config_key' => 'people_snd_info',
                            'loc_support_type' => 'header',
                            'loc_get_timezone_name_config_key' => 'people_loc_get_timezone_name',
                            'loc_get_datetime_format_config_key' => 'people_loc_get_datetime_format',
                            'loc_set_timezone_name_config_key' => 'people_loc_set_timezone_name',
                            'loc_set_datetime_format_config_key' => 'people_loc_set_datetime_format',
                            'space_connect_support_type' => 'header',
                            'space_connect_name_config_key' => 'people_space_connect_name',
                            'space_connect_auth_key_config_key' => 'people_space_connect_auth_key',
                            'content_support_type' => 'header',
                            'content_type_config_key' => 'people_content_type',
                            'content_src_path_config_key' => 'people_content_src_path',
                            'content_src_path_separator_config_key' => 'people_content_src_path_separator',
                            'auth_support_type' => 'header',
                            'auth_type_config_key' => 'people_auth_type',
                            'auth_user_profile_login_config_key' => 'people_auth_user_profile_login',
                            'auth_user_profile_pw_config_key' => 'people_auth_user_profile_pw',
                            'auth_user_profile_api_key_config_key' => 'people_auth_user_profile_api_key',
                            'auth_user_profile_token_config_key' => 'people_auth_user_profile_token',
                            'auth_app_profile_name_config_key' => 'people_auth_app_profile_name',
                            'auth_app_profile_secret_config_key' => 'people_auth_app_profile_secret',
                            'auth_app_profile_api_key_config_key' => 'people_auth_app_profile_api_key',
                            'auth_app_profile_token_config_key' => 'people_auth_app_profile_token',
                            'auth_super_admin_profile_name_config_key' => 'people_auth_super_admin_profile_name',
                            'auth_super_admin_profile_secret_config_key' => 'people_auth_super_admin_profile_secret'
                        ]
                    ]
                ]
            ]
        ]
    ]
);