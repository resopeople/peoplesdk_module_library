<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\request\info\factory\config\base\model\BaseConfigSndInfoFactory;
use people_sdk\library\requisition\request\info\factory\multi\model\MultiSndInfoFactory;
use people_sdk\module_library\requisition\request\helper\RequestHelper;



return array(
    // Requisition request sending information services
    // ******************************************************************************

    'people_requisition_request_snd_info_factory' => [
        'source' => MultiSndInfoFactory::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/requisition/request/snd_info_factory/config'],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_base_requisition_request_snd_info_factory' => [
        'source' => BaseConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/base/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Requisition request helper services
    // ******************************************************************************

    'people_requisition_request_helper' => [
        'source' => RequestHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_request_snd_info_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    SndInfoFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_requisition_request_snd_info_factory']
    ]
);